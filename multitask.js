process.env.UV_THREADPOOL_SIZE = 5

const https = require('https')
const crypto = require('crypto')
const fs = require('fs')

const start = Date.now()

const doRequest = () => {
  let i = 0
  const results = {}
  return () => {
    https
      .request('https://www.google.com', (res) => {
        res.on('data', () => {})
        res.on('end', () => {
          i++
          results[i] = Date.now() - start
          console.log(`https ${i}: `, Date.now() - start)
          console.log(results)
        })
      })
      .end()
  }
}
const requestFn = doRequest()

const doHash = () => {
  crypto.pbkdf2('a', 'b', 100000, 512, 'sha512', () => {
    console.log('Hash: ', Date.now() - start)
  })
}

requestFn()

fs.readFile('multitask.js', 'utf8', () => {
  console.log('fs: ', Date.now() - start)
})

doHash()
doHash()
doHash()
doHash()


